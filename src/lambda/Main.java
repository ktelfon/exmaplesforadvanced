package lambda;

import generic.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Person andyMurray = new Person("Andy", "Murray", 30);
        Predicate<Person> adultPersonTest = person -> person.getAge() >= 18;
        System.out.println(adultPersonTest.test(andyMurray));

        Runnable myRunnable = () -> System.out.println("Running a runnable");
        myRunnable.run();
        Predicate<String> startsWithABCTest = s -> s.startsWith("ABC");
        System.out.println(startsWithABCTest.test("ABCDEF"));
        System.out.println(startsWithABCTest.test("CDEF"));
        System.out.println(startsWithABCTest.test("23874CDEF"));

        // Method apply returns Integer and the type of the parameter is String
        Function<String, Integer> integerFunction = someString -> someString.length();
        System.out.println(integerFunction.apply("ABCDE"));
// Method apply returns String and the type of the parameter is String
        Function<String, String> replaceCommasWithDotsFunction = s -> s.replace(',', '.');
        System.out.println(replaceCommasWithDotsFunction.apply("a,b,c"));

        Person johnSmith = new Person("John", "Smith", 15);
        Predicate<Person> adultPersonTestRefe = Person::isAdult;
        System.out.println(adultPersonTestRefe.test(johnSmith));

        Supplier<Integer> randomNumberSupplier = () -> new Random().nextInt();
        int randomNumber = randomNumberSupplier.get();

        Consumer<Double> printWithPrefixConsumer = d -> System.out.println("Value: " + d);
        printWithPrefixConsumer.accept(10.5);

        UnaryOperator<Integer> toSquareUnaryOperator = i -> {
            int result = i * i;
            System.out.println("Result: " + result);
            return result;
        };

        toSquareUnaryOperator.apply(4);

        Optional<Integer> stringOptional1 = Optional.of(12);

        stringOptional1.isPresent();
        if(stringOptional1.isPresent()) {
            int value = stringOptional1.get();
            System.out.println(value);
        }
// Prints value if it is present
        int value = stringOptional1.orElse(6);
        System.out.println(value);

        List<String> names = Arrays.asList( "Brandon", "Michael", "Andrew");
        Stream<String> namesStream = names.stream();

        List<String> namesCopy = names.stream()
                .collect(Collectors.toList());

        // we use Optional, because stream may have no values
        Optional<String> firstName = names.stream().findFirst();
        firstName.ifPresent(System.out::println);

        Optional<String> firstNameAny = names.stream()
                .findAny();

        firstNameAny.ifPresent(System.out::println);


        List<String> namesStartingWithA = names.stream()
                .parallel()
                .filter(n -> n.startsWith("A"))
                .collect(Collectors.toList());
        System.out.println(namesStartingWithA);

        Function<Integer,Integer> sum = x-> x+x;
        Division div = (one,two)-> one/two;
        System.out.println("asdf");
        System.out.println(div.div(10,5));

        List<Integer> ints = Arrays.asList(1,2,3,4);
        int totalSum = 0;
        for (Integer anInt : ints) {
            totalSum = sum.apply(anInt);
        }
        System.out.println();

        String namesConcatenation = names.stream()
                .reduce("",
                        (currValue, element) -> (currValue.equals("") ? "" : currValue + ", ") + element);
        System.out.println(namesConcatenation);

    }


}
