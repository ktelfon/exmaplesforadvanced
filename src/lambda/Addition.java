package lambda;

public interface Addition {
    double add(double a1, double a2);
}
