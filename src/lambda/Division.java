package lambda;

public interface Division {
    double div(double a, double b);
}
