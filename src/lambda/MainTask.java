package lambda;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MainTask {
    public static void main(String[] args) {
        Division divisionOp = (a,b)-> a/b;
        System.out.println(divisionOp.div(10,5));

        Addition adOp = (a,b)->a+b;
        System.out.println(adOp.add(10,1));

        Operation operation =  (a,b)-> a/b;
        operation.arOper(10,5);
        operation =  (a,b)-> a+b;
        operation.arOper(10,1);
        operation =  (a,b)-> a*b;

        System.out.println("Reduce");
        List<Integer> ints = Arrays.asList(10,23,34,12);
        int sum = ints.stream().reduce(0,(cur,el)->cur + el);
        System.out.println(sum);
//        IntStream.of(10,23,12,31).

        List<String> listStr = new ArrayList<>();
        listStr.add("a");

        listStr = Arrays.asList("a","b","sdf");

        listStr = Stream.of("a","b v","l","l","o").collect(Collectors.toList());

        String[] split = "a".split("");
        Stream.of("a","b","l").forEach(System.out::println);
        Collections.sort(listStr);
        Set<String> setNames = new HashSet<>();

        setNames = listStr.stream().sorted().collect(Collectors.toSet());
        System.out.println(listStr
                .stream()
                .flatMap(s->Arrays.stream(s.split("\\s")))
                .count());

    }
}
