package lambda;

public interface Operation {
    double arOper(double a, double b);
}
