import carstuff.Car;
import carstuff.Vehicle;
import file.Person;

import java.io.*;

public class Main {
//
//    1. Create class Dog.
//    a) Add private fields to the class, like name, age, gender, race, weigth.
//    b) Create constructor that accepts all of the class fields.
//    c) Create additional constructor, that will accept only gender and race. It should call main
//    constructor with default values.
//            d) Create getters and setters for age and weigth.
//            e) Create object of class Dog. Verify if everything works as expected.
//    f) Add verification for all arguments passed to the setters. E.g. setWeigth method should
//    not accept values below or equal to 0.
//            2. Create class Pocket.
//    a) Add field „money”, create constructor, getter and setter.
//    b) Add verification for both getter and setter. Getter should result in returning as much
//    money, as the user asked for. It should return 0 if money <= 10.
//    c) Setter should not accept values below 0 and greater than 3000. It may print a message
//    like „I don’t have enough space in my pocket for as much money!”

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Dog dog = new Dog(false, "known");
        Dog dog1 = new Dog("Gogi", 1, false, "known", 10);

        System.out.println(dog.getWeight());

        dog.setWeight(-10);

        System.out.println(dog.getWeight());

        Pocket p = new Pocket(11);
        System.out.println(p.getMoney());
        p.setMoney(300);
        System.out.println(p.getMoney());

        Car car = new Car(100, true);

        Vehicle v = new Car(1000, true);

        System.out.println(dog);
        printMaxSpeed(v);
        printMaxSpeed(car);

        printWithPrefix("1", v);
        printWithPrefix("1", car);
        printWithPrefix("1", dog);
        printWithPrefix("1", "asdf");
        printWithPrefix("1", 1);

    }

    private static void printMaxSpeed(Vehicle vehicle) {
        System.out.println(vehicle.getMaxSpeed());
    }

    private static void printWithPrefix(String prefix, Object object) {
        System.out.println(prefix + object);
    }
}
