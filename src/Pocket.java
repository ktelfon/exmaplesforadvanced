public class Pocket {
    private double money;

    public Pocket(double money) {
        this.money = money;
    }

    public double getMoney() {
        if (this.money <= 10)
            return 0;
        return money;
    }

    public void setMoney(double money) {
        if (money > 0 && money < 3000) {
            this.money = money;
        }else {
            System.out.println("Wrong amount");
        }
    }
}
