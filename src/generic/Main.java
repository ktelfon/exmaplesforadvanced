package generic;

import carstuff.Car;
import carstuff.Moto;
import carstuff.Vehicle;
import parents.animals.Dog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Car car = new Car(123, true);
        Car car1 = new Car(1232, true);
        Car car2 = new Car(12, true);
        Dog g = new Dog();
        GenericBox<Car> boxWithACarInIt = new GenericBox<>(car);
        GenericBox<Dog> boxWithADogInIt = new GenericBox<>(g);
        GenericBox<String> boxWithATextInIt = new GenericBox<>("sadfsadfd");

        Moto moto = new Moto();
        Garage<Car> garArCar = new Garage<>(car);
        Garage<Moto> garArMoto = new Garage<>(moto);

        Garage<Vehicle> garArSomVe = new Garage<>(car);

        garArCar.repairVehicle();

        if(car1.compareTo(car2) > 0) {
            System.out.println("car1 is faster!");
        }

        List<Car> carList = new ArrayList<>();




        GenericBox<String> boxWithString = new GenericBox<>("asdf");
        GenericBox<Car> boxWithCar = new GenericBox<>(car);
        GenericBox<Integer> boxWithInteger = new GenericBox<>(1);
        GenericBox<Double> boxWithDouble = new GenericBox<>(1.0);


        GenericBox[] boxArray = {boxWithACarInIt, boxWithCar, boxWithInteger, boxWithDouble};
        GenericBox[] boxArray1 = new GenericBox[4];
        boxArray1[0] = boxWithACarInIt;
        boxArray1[1] = boxWithCar;

        List<GenericBox> boxList = new ArrayList<>();
        boxList.add(boxWithDouble);
        boxList.add(boxWithADogInIt);
        boxList.add(boxWithString);
        for (GenericBox genericBox : boxList) {

        }

        System.out.println("+++++++");
        List<String> visitedCountries = new ArrayList<>();
        visitedCountries.add("Germany");
        visitedCountries.add("France");
        visitedCountries.add("Spain");
        visitedCountries.remove("France");

        System.out.println(visitedCountries.contains("Germany") + " RESULT");
// prints Germany Spain, as "France" was removed
        for (String country : visitedCountries) {
            System.out.println(country + " ");
        }
        System.out.println("+++++++");



        Iterator<String> iterator = visitedCountries.iterator();
// prints Germany Spain, as "France" was removed
        System.out.println("--------");
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }
        System.out.println("--------");

        for (GenericBox genericBox : boxArray) {
            System.out.println(genericBox);
        }

    }
}
