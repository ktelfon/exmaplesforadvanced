package generic;

public class Person implements Comparable<Person> {

    private double height;
    private String name;
    private int age = 30;

    public int getAge() {
        return age;
    }

    public Person(String andy, String murray, double height) {
        this.height = height;
    }

    @Override
    public int compareTo(Person o) {
        double result = this.height - o.height;
        if(result == 0){
            return 0;
        }
        if(result > 0){
            return 1;
        }else {
            return -1;
        }
    }

    public static boolean isAdult(Person person) {
        return false;
    }
}
