package parents.animals;

public class Dog extends Animal{

    private Muzzle muzzle;

    public void setMuzzle(Muzzle muzzle) {
        this.muzzle = muzzle;
    }

    public void yieldVoice() {
        System.out.println("bar");
    }


    public void muzzleInfo() {
        muzzle.info();
    }
}
