package parents.animals;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Cat c= new Cat();
        Dog d = new Dog();

        c.yieldVoice();
        d.yieldVoice();

        Animal[] animals = new Animal[3];

        List<Animal> animals1 = new ArrayList<>();
        animals1.add(c);

        animals[0] = c;
        animals[1] = d;

        for (Animal a : animals1) {
            System.out.println("A " + a.getClass().getSimpleName()+" goes");
            a.yieldVoice();

        }

        Muzzle muzzle = new Muzzle();














        d.setMuzzle(muzzle);
        d.muzzleInfo();
    }
}
