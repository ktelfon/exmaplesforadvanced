package parents.animals;

public abstract class Animal {

    public abstract void yieldVoice();
}
