package parents.animals;

public class Muzzle {
    private String color = "red";

    public void info(){
        System.out.println("red");
    }

    @Override
    public String toString() {
        return "Muzzle{" +
                "color='" + color + '\'' +
                '}';
    }
}
