package parents.shapes;

public class PrintingException extends Exception {
    public PrintingException(String ohNo) {
        super(ohNo);
    }
}
