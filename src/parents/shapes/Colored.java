package parents.shapes;

public interface Colored {
    void color();
}
