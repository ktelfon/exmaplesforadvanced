package parents.shapes;

public interface ShapeI {

    double getArea();
    double getPerimeter();

    default void print() {
        System.out.println("Shape: " + this);
    }

}
