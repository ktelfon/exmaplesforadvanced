package parents.shapes;

public class Circle implements ShapeI, Colored {

    private double a;
    private double b;

    public Circle() {

    }

    @Override
    public double getArea() {
        return a * b;
    }
    @Override
    public double getPerimeter() {
        return 2 * a + 2 * b;
    }

    @Override
    public void color() {

    }
}
