package parents.enums;

public enum LengthUnit {
    METER(1, "Meter unit"),
    CENTIMETER(0.01, "Centimeter unit");
    double value;
    String prettyName;
    LengthUnit(double value, String prettyName) {
        this.value = value;
        this.prettyName = prettyName;
    }
    @Override
    public String toString() {
        return prettyName;
    }
}
