package parents.enums;

public enum Planets {
    MERCURY(1, "mid merc"),
    NINE(1, "unknown"),
    VENUS(123, "big ven");

    private double distance;
    private String customText;

    Planets(double distance, String customText) {
        this.distance = distance;
        this.customText = customText;
    }

    public double distanceFromEarth(){
        System.out.println(distance);
        return distance;
    }

    @Override
    public String toString() {
        return customText;
    }

    //Venus
// Earth
// Mars
// Jupiter
// Saturn
// Uranus
// Neptune
}
