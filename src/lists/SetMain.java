package lists;

import java.util.*;

public class SetMain {
    public static void main(String[] args) {

        Set<String> travelRoute = new HashSet<>();
        Set<String> travelRouteTree = new TreeSet<>();

        travelRoute.add("Berlin");
        travelRoute.add("Paris");
        travelRoute.add("Madrid");
        travelRoute.add("Paris");
        travelRoute.add("Berlin");
        travelRoute.add("Riga");
        travelRoute.add("Jelgava");
        travelRoute.add("Ogre");
        travelRoute.add("Tel Aviv");
        travelRoute.remove("Paris");

        travelRouteTree.add("Berlin");
        travelRouteTree.add("Paris");
        travelRouteTree.add("Madrid");
        travelRouteTree.add("Paris");
        travelRouteTree.add("Berlin");
        travelRouteTree.remove("Paris");
// prints Berlin Madrid, order may differ
// as set is not ordered
//        for (String country : travelRoute) {
//            System.out.print(country + " ");
//        }

//        Arrays.sort();
        List<String> listToSort = new ArrayList<>(travelRoute);

        Collections.sort(listToSort);
        System.out.println(listToSort);
        Collections.reverse(listToSort);
        System.out.println(listToSort);
        Collections.shuffle(listToSort);
        System.out.println(listToSort);
        Collections.swap(listToSort,1,2);
        System.out.println(listToSort);

//        List<>

    }
}
