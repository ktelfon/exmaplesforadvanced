package lists;

import parents.animals.Dog;

import java.util.*;

public class MapMain {

    public static void main(String[] args) {
        Map<String, String> countries = new HashMap<>();

        countries.put("Poland", "Warsaw");
        countries.put("Germany", "Berlin");
        countries.put("Germany", "Munhen");


        for(Map.Entry<String, String> dictionary: countries.entrySet()) {
            String country = dictionary.getKey();
            String capital = dictionary.getValue();
            System.out.printf("%s : %s\n", country, capital);
            System.out.println(country + capital);
        }

        Map<Dog,String> mapmapmap= new HashMap<>();
        Dog key = new Dog();
        mapmapmap.put(key,"map MAP");
        System.out.println(mapmapmap.get(key));

        Map<String, String> mapOfNames = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String firstName = sc.nextLine();
        System.out.println("Enter Last Name: ");
        String lastName = sc.nextLine();
        mapOfNames.put(firstName,lastName);

        Map<String, Integer> mapOfAges = new HashMap<>();
        mapOfAges.put("NAME", 10);

        Map<String, List> mapOfFriends = new HashMap<>();

        List<String> listOfFriends = new ArrayList<>();
        listOfFriends.add("new draugs");
        listOfFriends.add("old draugs");
        listOfFriends.add("not so old draugs");

        mapOfFriends.put("Draugs this is an name",listOfFriends);

        for(Map.Entry<String, List> dictionary: mapOfFriends.entrySet()) {
            String name = dictionary.getKey();
            List listOfFriendNames = dictionary.getValue();

            System.out.println(name + ": " +listOfFriendNames);
        }

        Map<String,Map<String,String>> db = new HashMap<>();

        Map<String,String> details = new HashMap<>();
        details.put("id","123-0123");
        details.put("id","123-0123");
        details.put("id","123-0123");
        details.put("id","123-0123");

        db.put("dfhhdh",details);

        for(Map.Entry<String, Map<String, String>> dictionary: db.entrySet()) {
            String name = dictionary.getKey();
            Map<String, String> details2 = dictionary.getValue();

            System.out.println("My name is " + name);
            for (Map.Entry<String, String> entry : details2.entrySet()) {
                System.out.println(" " + entry.getKey() + ":" + entry.getValue());
            }

        }


    }
}
