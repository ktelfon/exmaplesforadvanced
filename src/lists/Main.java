package lists;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        List<String> cart = new LinkedList<>();
        cart.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {



                return -1;
            }
        });

        

// a)
        while (cart.size() < 4) {
            System.out.println("Enter item: ");
            String input = sc.nextLine();
            System.out.println("You have entered " + input);

            if(cart.contains(input)){
                continue;
            }

            cart.add(input);
        }
        System.out.println(cart);
        Collections.sort(cart);
// b)
        System.out.println("Anything to delete ? (yes)");
        String command = sc.nextLine();
        if(command.equals("yes")){
            System.out.println("What ?");
            String itemToDelete = sc.nextLine();
            cart.remove(itemToDelete);
        }
        System.out.println(cart);

//  c)
        String prev = "";
        for (String s : cart) {
            if(s.startsWith("m")){

                System.out.println(prev);

                System.out.println(s);
                int index = cart.indexOf(s);
                String previousItem = cart.get(index - 1);
                System.out.println(previousItem);
            }
            prev = s;
        }
//  d)
        List<Double> ratings = new ArrayList<>();
        double sum = 0;
        while (ratings.size() < 5) {
            System.out.println("Enter A rating:");
            double input = sc.nextDouble();
            if(input >= 1 && input <= 6) {
                ratings.add(input);
                sum += input;
            }
        }
        System.out.println("Avg:" + (sum / ratings.size()));

        List<String> one = new ArrayList<>();
        one.add("1x1=1");

        List<List<String>> table = new ArrayList<>();
        table.add(one);

        List<String> integers = table.get(0);
        String integer = integers.get(0);
    }
}
