package file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileReaderTaskResult {
    public static void main(String[] args) throws IOException, InterruptedException {

        Thread.sleep(1000);

        Path path = Paths.get("text.txt");
        List<String> fileLines = Files.readAllLines(path);

        fileLines
                .stream()
                .flatMap(ar-> Arrays.stream(ar.split("\\s")))
                .filter(s->!s.equals(""))
                .forEach(System.out::println);

        Pattern p = Pattern.compile("(\\w+)");
        int count = 0;
        for (String fileLine : fileLines) {
            Matcher m = p.matcher(fileLine);
            while (m.find()) {
                count++;
            }
        }
        System.out.println();
        System.out.println(count + " words");
        System.out.println();
        count = 0;
        p = Pattern.compile("(\\W)");
        for (String fileLine : fileLines) {
            Matcher m = p.matcher(fileLine);
            while (m.find()) {
                count++;
            }
        }
        System.out.println();
        System.out.println(count + " special chars");
        System.out.println();
        count = 0;
        p = Pattern.compile("([H|h]er)");
        for (String fileLine : fileLines) {
            Matcher m = p.matcher(fileLine);
            while (m.find()) {
                count++;
            }
        }
        System.out.println();
        System.out.println(count + " her");
        System.out.println();
    }
}
