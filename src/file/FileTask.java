package file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileTask {
    public static void main(String[] args) {
        Path txtFile = Paths.get("text.txt");
        List<String> txt = null;
        try {
            txt = Files.readAllLines(txtFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(txt);
    }
}
