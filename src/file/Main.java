package file;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File absoluteFile = new File("C:\\Users\\Deniss.Fjodorovs\\Documents\\Java SDA projects\\Exmaples\\myFile.txt");
        File relativeFile = new File("files\\out.txt");

        List<String> resilr= new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(relativeFile))) {
            String fileLine;
            while ((fileLine = bufferedReader.readLine()) != null) {
                resilr.add(fileLine);
                System.out.println(fileLine);
            }
        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(relativeFile))) {
            String fileLine = "file line";
            bufferedWriter.write(fileLine);
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(relativeFile, true))) {
            String fileLine = "\nappended file line";
            bufferedWriter.write(fileLine);
        }

        Person person = new Person("Michael", "Dudikoff");
        try (FileOutputStream fileOutputStream = new FileOutputStream(relativeFile);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(person);
        }

        person.setFirstName("asdfsdf");
        try (FileInputStream fileInputStream= new FileInputStream(relativeFile);
             ObjectInputStream objectInputStream =
                     new ObjectInputStream(fileInputStream)) {
            person = (Person) objectInputStream.readObject();
        }
        System.out.println(person);

        Path absolutePath = Paths.get("C:\\Users\\Deniss.Fjodorovs\\Documents\\Java SDA projects\\Exmaples\\myFileNew.txt");
        Path relativePath = Paths.get("new.txt");

        List<String> fileLines = Files.readAllLines(absolutePath);
        System.out.println("1" + fileLines);
        List<String> fileLines1 = Files.readAllLines(absolutePath, Charset.forName("UTF-8"));
        System.out.println("2" + fileLines1);
        List<String> fileLines2 = Arrays.asList("first line", "second line");
        System.out.println("3" + fileLines2);
        Files.write(absolutePath, fileLines);
        Files.write(absolutePath, fileLines, StandardOpenOption.APPEND);
        Path a = Paths.get("C:\\Users\\Deniss.Fjodorovs\\Documents\\Java SDA projects\\Exmaples\\df");
        Files.createDirectory(a);



    }
}
