package threads.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Employee implements Runnable, Comparable<Employee> {
    private String name;
    private volatile boolean flag = true;
    // working timeout
    private int timeout = 2;

    public Employee(String name) {
        this.name = name;
    }

    public void workFaster(){
        timeout--;
    }

    public String getName() {
        return name;
    }

    public void goHome() {
        flag = false;
    }

    @Override
    public void run() {
        // start working
        SimpleDateFormat sdf = new SimpleDateFormat("HH:MM");
        System.out.println(name + ": I came to work at " + sdf.format(new Date()));

        // working
        while (flag) {
            System.out.println(name + " Still working");
            try {
                TimeUnit.SECONDS.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //end working
        System.out.println(name + ": " + sdf.format(new Date()) + " it's time to go home!");
    }

    // NEEDED for SORTING
    @Override
    public int compareTo(Employee o) {
        //compare by name
        return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", flag=" + flag +
                ", timeout=" + timeout +
                '}';
    }
}
