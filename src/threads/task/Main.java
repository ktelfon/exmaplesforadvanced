package threads.task;

import generic.Garage;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Employee susi = new Employee("Susi");
        Employee frank = new Employee("Frank");

        List<Employee> employeeListShorterInit = new ArrayList<>(Arrays.asList(
                new Employee("Givi"),
//                susi,
//                frank,
                new Employee("Gogi")
        ));

        for (Employee employee : employeeListShorterInit) {
            Thread t = new Thread(employee);
            t.start();
        }

        TimeUnit.SECONDS.sleep(1);

        System.out.println(employeeListShorterInit);

        // Sorting a-z
        Collections.sort(employeeListShorterInit);
        System.out.println(employeeListShorterInit);

        // Sorting z-a
        employeeListShorterInit.sort(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o2.getName().compareTo(o1.getName());
            }
        });
        System.out.println(employeeListShorterInit);

        while (employeeListShorterInit.size() > 0){
            sendEmployeeHome(employeeListShorterInit);
            // making all of employees work faster
            for (Employee employee : employeeListShorterInit) {
                employee.workFaster();
            }
            TimeUnit.SECONDS.sleep(4);
        }
    }

    // Removing Employee that is going home
    public static List<Employee> sendEmployeeHome(List<Employee> employeeList){
        for (Employee employee : employeeList) {
            employee.goHome();
            employeeList.remove(employee);
            System.out.println("___");
            System.out.println(employeeList);
            break;
        }
        return employeeList;
    }
}
