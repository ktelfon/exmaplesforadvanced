package threads;

public class Bench {
    private int availableSeats;

    public Bench(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public void takeASeat() {
        System.out.println("Taking a seat.");

        synchronized (this) {
            if (availableSeats > 0) {
                availableSeats--;
                System.out.println("Free seats left " + availableSeats);
            } else {
                System.out.println("There are no available seats. :(");
            }
        }
    }
}
