package threads;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws InterruptedException{
//        Bench bench = new Bench(1); // creating bench with one free seat
//        SeatTakerThread seatTaker1 = new SeatTakerThread(bench);
//        SeatTakerThread seatTaker2 = new SeatTakerThread(bench);
//        seatTaker1.start();
//        seatTaker2.start();
        JFrame f=new JFrame();//creating instance of JFrame

        JButton b=new JButton("click");//creating instance of JButton
        b.setBounds(130,100,100, 40);//x axis, y axis, width, height

        f.add(b);//adding button in JFrame

        f.setSize(400,500);//400 width and 500 height
        f.setLayout(null);//using no layout managers
        f.setVisible(true);//making the frame visible

        StopWatchThread st = new StopWatchThread(65);
        StopWatchThread st1 = new StopWatchThread(11165);
        Thread stopWatchThread = new Thread(st);
        Thread stopWatchThread1 = new Thread(st1);
        Thread stopWatchThread2 = new Thread(st);
        stopWatchThread.start();
        stopWatchThread2.start();
        stopWatchThread1.start();

        Thread.sleep(30000);

        st.stopThis();
        st1.stopThis();

    }
}
