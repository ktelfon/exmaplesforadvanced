package threads;

import java.util.concurrent.atomic.AtomicBoolean;

public class StopWatchThread implements Runnable {

    private int anInt;
    private volatile boolean flag = true;

    public StopWatchThread(int i) {
        this.anInt = i;
    }

    public void stopThis(){
        flag = false;
    }

    private void decrement() {
        anInt--;
    }

    @Override
    public void run() {
        while (flag) {
            System.out.println("Hello? " + anInt);
            decrement();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}


