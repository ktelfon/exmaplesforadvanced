package exception;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("int -> " + sc.nextInt());
        } catch (Exception e) {
            try {
                System.out.println("double -> " + sc.nextDouble());
            } catch (Exception ex) {
                System.out.println("not number");
            }
        }
    }
}
