public class Dog {
    private String name = "Rex";
    private int age = 1;
    private boolean male = true;
    private String race = "uknown";
    private int weight = 1;

    public Dog(int age, boolean male, String race) {
        this.age = age;
        this.male = male;
        this.race = race;
    }

    public Dog(String name, int age, boolean male, String race, int weight) {
        this.name = name;
        this.age = age;
        this.male = male;
        this.race = race;
        this.weight = weight;
    }

    public Dog(boolean male, String race) {
        this.male = male;
        this.race = race;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if(weight > 0) {
            this.weight = weight;
        }
    }
}
