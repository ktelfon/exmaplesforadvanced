package carstuff;

import carstuff.Vehicle;

public class Car extends Vehicle implements Comparable<Car>  {
    private boolean convertible;
    public Car(int maxSpeed, boolean convertible) {
        super(maxSpeed);
        this.convertible = convertible;
    }
    public boolean isConvertible() {
        return convertible;
    }

    @Override
    public void move() {

    }

    @Override
    public String toString(){
        String s = "carstuff.Car ";
        return s + super.toString();
    }

    @Override
    public void repair() {
        System.out.println("REPAIRING!");
    }

    @Override
    public int compareTo(Car otherCar) {
        return this.maxSpeed - otherCar.maxSpeed;
    }
}
