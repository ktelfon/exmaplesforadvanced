package carstuff;

public class Moto extends Vehicle {

    public Moto() {
        super(Integer.MAX_VALUE);
    }

    @Override
    public void move() {

    }

    @Override
    public void repair() {
        System.out.println("NO!");
    }
}
