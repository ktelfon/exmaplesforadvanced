package carstuff;

public abstract class Vehicle {
    protected int maxSpeed;
    public Vehicle(int maxSpeed) {
        System.out.println("V!");
        this.maxSpeed = maxSpeed;
    }
    public int getMaxSpeed() {
        return maxSpeed;
    }
    public abstract void move();


    @Override
    public String toString() {
        return "carstuff.Vehicle{" +
                "maxSpeed=" + maxSpeed +
                '}';
    }

    public abstract void repair();
}
